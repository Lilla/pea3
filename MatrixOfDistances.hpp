//
//  MatrixOfDistances.hpp
//  algorytm_genetyczny
//
//  Created by Lila Łomnicka on 1/12/19.
//  Copyright © 2019 Lila. All rights reserved.
//

#ifndef MatrixOfDistances_hpp
#define MatrixOfDistances_hpp




#include <iostream>
#include <fstream>

using namespace std;

class MatrixOfDistances{
    int citiesNumber;
    int ** distances;
    bool flagGood;
    
public:
    MatrixOfDistances();
    MatrixOfDistances(int);
    ~MatrixOfDistances();
    MatrixOfDistances(const MatrixOfDistances&);
    bool fileReadLine(std::ifstream &, int , int );
    
    
    void init(int);
    void display();
    int getCityNumber() const;
    int getDistance(int, int) const;
    int reduceCostColumn();
    int reduceCostRow();
    void reduceMatrix(int, int);
    bool isGood();
    void loadFromfile(string);
    void randomMatrix(int);
    int getMatrixSize(string );
    bool loadFile(string);
    
    //metoda która kopiuje macierz i zwraca wskaźnik do skopiowanej macierzy
    MatrixOfDistances* copy() const;
};





#endif /* MatrixOfDistances_hpp */
