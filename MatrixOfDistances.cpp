//
//  MatrixOfDistances.cpp
//  algorytm_genetyczny
//
//  Created by Lila Łomnicka on 1/12/19.
//  Copyright © 2019 Lila. All rights reserved.
//

#include "MatrixOfDistances.hpp"
#include <sstream>

using namespace std;
MatrixOfDistances::MatrixOfDistances(){
    this->distances=nullptr;
    this->citiesNumber = 0;
    this->flagGood = false;
}
MatrixOfDistances::MatrixOfDistances(int cityNumb){
    this->init(citiesNumber);
    this->flagGood = false;
}
MatrixOfDistances::~MatrixOfDistances(){
    for (int i=0; i<this->citiesNumber; i++){
        delete [] this->distances[i];
    }
    
    delete [] this->distances;
}
MatrixOfDistances::MatrixOfDistances(const MatrixOfDistances& matrix){
    int cityNumber = matrix.citiesNumber;
    this->init(cityNumber);
    for(int i=0;i<cityNumber;i++){
        ::copy(&matrix.distances[i][0] ,&matrix.distances[i][cityNumber], &this->distances[i][0]);
    }
}

void MatrixOfDistances::init(int citiesNumber){
    this->citiesNumber=citiesNumber;
    this->distances= new int*[citiesNumber];
    for(int i=0; i<citiesNumber; i++){
        this->distances[i]=new int[citiesNumber];
    }
}
void MatrixOfDistances:: display(){
    for (int i=0; i<this->citiesNumber; i++){
        for(int j=0; j<this->citiesNumber; j++)
        {
            cout<<this->distances[i][j]<<" ";
        }
        cout<<endl;
        
    }
}
int MatrixOfDistances::getCityNumber() const{
    return this->citiesNumber;
}
int  MatrixOfDistances:: getDistance (int startCity, int destination) const{
    return this->distances[startCity][destination];
}
int MatrixOfDistances:: reduceCostRow(){
    int minValue;
    int cost=0;
    
    for(int i=0; i<this->citiesNumber; i++){
        minValue=INT_MAX;
        for(int k=0; k<this->citiesNumber; k++){
            if(minValue>this->distances[i][k] && this->distances[i][k]>=0){
                minValue=this->distances[i][k];
            }
        }
        for(int k=0; k<this->citiesNumber; k++){
            if( minValue>0 && minValue!=INT_MAX){
                this->distances[i][k]=this->distances[i][k]-minValue;}
        }
        if(minValue!=INT_MAX && minValue>0)
            cost=+minValue;
        
    }
    return cost;
}
int MatrixOfDistances::reduceCostColumn(){
    int minValue;
    int cost=0;
    for(int i=0; i<this->citiesNumber; i++){
        minValue=INT_MAX;
        for(int k=0; k<this->citiesNumber; k++){
            if(minValue>this->distances[k][i] && this->distances[k][i]>=0){
                minValue=this->distances[k][i];
            }
            
        }
        if(minValue!=INT_MAX){
            cost=+minValue;}
        
        
        for(int k=0; k<this->citiesNumber; k++){
            if( this->distances[k][i]>0 && this->distances[k][i]!=0){
                this->distances[k][i]=this->distances[k][i]-minValue;}
        }
        
    }
    return cost;
    
}

void MatrixOfDistances::reduceMatrix(int startCity, int destination){
    
    for(int i=0; i<this->citiesNumber; i++){
        this->distances[startCity][i]=-1;
        this->distances[i][destination]=-1;
    }
    
    this->distances[destination][startCity]=-1;
}
bool MatrixOfDistances:: isGood(){
    
    return this->flagGood;
}

MatrixOfDistances* MatrixOfDistances::copy()const{
    
    return new MatrixOfDistances(*this);
    
}

/*void MatrixOfDistances::loadFromfile(string name){
 
 // wczytanie danych z pliku i inicjalizacja potrzebnych nam zmiennych
 fstream file;
 // file.open("/Users/lilalomnicka/Desktop/Sem5/PEA/pea2/pea2/dane/"+name, ios::in);
 string cost;
 
 citiesNumber=getMatrixSize(name);
 cout<<citiesNumber;
 this->init(citiesNumber);
 for(int i=0; i<citiesNumber;++i){
 for(int j=0; i<citiesNumber;++i){
 file>>cost;
 std:: istringstream iss(cost);
 int n;
 while(iss>>n){
 int k=1;
 //  if((i!=0 )&& (j!=0)){
 distances[i][j]=n;
 cout<<k<<" n: "<<n<<endl;
 cout<<"distances: "<<distances[i][j]<<endl;
 k++;
 // }
 }
 }
 }
 }
 */

bool MatrixOfDistances::fileReadLine(std::ifstream &input, int size, int numberRow)
{
    std::string s;
    std::getline(input, s);
    
    if(input.fail() || s.empty())
        return false;
    
    std::istringstream in_ss(s);
    
    for (int i = 0; i < size; i++)
    {
        in_ss >> distances[numberRow][i];
        if(in_ss.fail())
            return (false);
    }
    
    return true;
}

bool MatrixOfDistances::loadFile(string name)
{
    int number;
    this->flagGood = false;
    std::ifstream input("/Users/lilalomnicka/Desktop/Sem5/PEA/pea2/pea2/"+name, std::ios::in);
    
    if (input.good())
    {
        input >> number;
        // po wczytaniu wyczysc linie
        input.ignore(INT_MAX, '\n');
        
        if(input.fail())
        {
            input.close();
            return false;
        }
        // usuwa i tworzy nowa macierz
        this->~MatrixOfDistances();
        this->init(number);
        for (int i=0; i<number; i++)
        {
            // jesli wczytalo zle to koniec
            if (!fileReadLine(input, number, i))
            {
                input.close();
                return false;
            }
        }
        input.close();
        this->flagGood = true;
        return true;
    }
    
    return false;
}



void MatrixOfDistances::randomMatrix(int citiesNumb){
    srand(time(NULL));
    //zainicjalizowanie macierzy kwadratowej o rozmiarze takim jak ilość miast, gotowej do inicjalizowania randomowymi wartościami
    this->~MatrixOfDistances();
    this->init(citiesNumb);
    for(int j=0; j<this->citiesNumber;j++){
        for(int i=0; i<this->citiesNumber;i++)
        {
            this->distances[j][i]=rand()%50+2;
            if(i==j){
                this->distances[j][i]=-1;
            }
        }
    }
    for (int i=0; i<this->citiesNumber; i++){
        for(int j=0; j<this->citiesNumber; j++)
        {
            cout<<this->distances[i][j]<<" ";
        }
        cout<<endl;
    }
}

