//
//  Tsp_genetic.cpp
//  algorytm_genetyczny
//
//  Created by Lila Łomnicka on 1/12/19.
//  Copyright © 2019 Lila. All rights reserved.
//

#include "Tsp_genetic.hpp"
Tsp_genetic:: ~Tsp_genetic(){}
void Tsp_genetic::init(){
    vector<Path>().swap(population);
    citiesNumber=matrix.getCityNumber();
    
}
const Path Tsp_genetic::randomPath(){
    vector<int> path;
    for(int i=0; i<citiesNumber; i++){
        path.push_back(i);
    }
    random_shuffle(path.begin(), path.end());
    
    return {-1,move(path)};
}

double Tsp_genetic::getRandomFromZeroToOne()
{
    return ((double)rand() / (double)RAND_MAX);
}

int Tsp_genetic:: countCost(vector<int> &path){
    int cost=0;
    
    for(int i=0; i<path.size()-1; i++){
        cost+=matrix.getDistance(path[i],path[i+1]);
    }
    cost+=matrix.getDistance(path[path.size()-1], path[0]);
    
    return cost;
}

void Tsp_genetic::mutate(Path &element){
    int randomVertOne, randomVertTwo;
    
    randomVertOne=rand()%citiesNumber;
    randomVertTwo=rand()%citiesNumber;
    
    do{
        randomVertTwo=rand()%citiesNumber;
    }while(randomVertTwo==randomVertOne);
        return swap(element.path[randomVertOne],element.path[randomVertTwo]);
}

void Tsp_genetic::initPopulation(int populationSize){
    population.resize(populationSize);
    int firstVertex=1;
    population[0]=greedy(firstVertex);
    for(int i=1; i<populationSize; i++){
        population[i]=randomPath();
    }
}

void Tsp_genetic::countFitness(){
   for ( auto& el: population){
        el.cost=countCost(el.path);
    }
    
}
const Path Tsp_genetic::greedy(int start){
    // algorytm zachlany
    // wybiera najkrotsza sciezke do nie odwiedzonego wierzcholka i tak az odwiedzi wszystkie
    vector<int> path(citiesNumber);
    bool visited[citiesNumber];
    for(int i=0; i<citiesNumber;i++){
        visited[i]=false;
    }
    int min, minNumber, tmp, cost = 0;
    
    path[0] = start;
    visited[start] = true;
    
    // dla kazdego wierzcholka szukamy najkrotszej do nieodwiedzonego i tam idziemy
    for (int i=1; i<citiesNumber; i++)
    {
        min = INT_MAX;
        minNumber = 0;
        for (int j=0; j<citiesNumber; j++)
        {
            // odwiedzony albo sam do siebie
            if (visited[j] || path[i-1] == j)
                continue;
            
            tmp = matrix.getDistance(path[i-1], j);
            if (tmp < min)
            {
                min = tmp;
                minNumber = j;
            }
        }
        visited[minNumber] = true;
        path[i] = minNumber;
        cost += min;
    }
    cost += matrix.getDistance(path[citiesNumber-1], start);
    
    return {-1, move(path)};
}

Path Tsp_genetic::selectionUsingTournamentMethod(){
    
    Path randomIndividual;
    Path bestIndividual;
    vector<int> visited;
    bool isVisited;
    int tmp;
    
    
    tmp=rand()%populationSize;
    visited.push_back(tmp);
    bestIndividual=population[tmp];
    
    for(int i=0; i<(numberOfTurnament)-1; i++){
        do{
           isVisited=false;
            tmp=rand()%populationSize;
            for(int el: visited){
                if(el==tmp){
                    isVisited=true;
                    break;
                }
                
            }
           
        }while (isVisited);
    
    visited.push_back(tmp);
    randomIndividual=population[tmp];
    
    if(randomIndividual.cost<bestIndividual.cost){
        bestIndividual=randomIndividual;
    }
    }
        
    return bestIndividual;
}
    


Path Tsp_genetic:: doCrossoverPMX(vector<int> &firstParent, vector<int> &secondParent){
    int smallerNumberOfRange;
    int biggerNumberOfRange;
    std::vector<int> child(citiesNumber, -1);
    std::vector<int>::iterator iteratorVector;
    int tmpCounter;
    int tmpNumber;
    
    smallerNumberOfRange=rand()%citiesNumber;
    do{
        biggerNumberOfRange=rand()%citiesNumber;
    }while(smallerNumberOfRange==biggerNumberOfRange);
    
    if(smallerNumberOfRange>biggerNumberOfRange){
        swap(smallerNumberOfRange,biggerNumberOfRange);
    }
    
    
    std::copy(&firstParent[smallerNumberOfRange], &firstParent[biggerNumberOfRange] + 1, &child[smallerNumberOfRange]);
    
    for (int i = smallerNumberOfRange; i <= biggerNumberOfRange; i++)
    {
        iteratorVector = std::find(child.begin(), child.end(), secondParent[i]);
        
        
        if (iteratorVector == child.end())
        {
           
            tmpCounter = i;
            do
            {
                tmpNumber = firstParent[tmpCounter];
                for (tmpCounter=0; tmpCounter<citiesNumber; tmpCounter++)
                    if (tmpNumber == secondParent[tmpCounter])
                        break;
                
            } while (child[tmpCounter] != -1);
            
            child[tmpCounter] = secondParent[i];
        }
    }
    
    
    tmpCounter=0;
    while (tmpCounter < citiesNumber)
    {
      
        while (tmpCounter < citiesNumber && (std::find(child.begin(), child.end(), secondParent[tmpCounter]) != child.end())) tmpCounter++;
        
        
        iteratorVector = std::find(child.begin(), child.end(), -1);
        if (iteratorVector != child.end())
            *iteratorVector = secondParent[tmpCounter];
    }
    
    return {-1, move(child)};
    
  /*  vector<int> child;
    child.resize(citiesNumber);
    int rangeNumberOne, rangeNumberTwo;
    
    vector<int> child2;
    child2.resize(citiesNumber);
    vector<int> replacement1;
    replacement1.resize(citiesNumber, -1);
    vector<int> replacement2;
    replacement2.resize(citiesNumber, -1);
    
    
    
    rangeNumberOne=rand()%citiesNumber;
    do{
        rangeNumberTwo=rand()%citiesNumber;
    }while(rangeNumberOne==rangeNumberTwo);
    
    if(rangeNumberOne>rangeNumberTwo){
        swap(rangeNumberOne,rangeNumberTwo);
    }
    
    for(int i=rangeNumberOne; i<rangeNumberTwo+1; i++){
        child[i]=parent2[i];
        child2[i]=parent1[i];
        
        replacement1[parent2[i]]=parent1[i];
        replacement2[parent1[i]]=parent2[i];
    }
    
    for(int i=0; i<citiesNumber; i++){
        if((i<rangeNumberOne)|| (i>rangeNumberTwo)){
            int n1=parent1[i];
            int m1=replacement1[n1];
            
            int n2=parent2[i];
            int m2=replacement2[n2];
            
            while(m1!=-1){
                n1=m1;
                m1=replacement1[m1];
            }
            
            while(m2!=-1){
                n2=m2;
                m2=replacement2[m2];
            }
            
            child[i]=n1;
            child2[i]=n2;
        }
    }
    
    child.clear();
    replacement2.clear();
    replacement1.clear();
    

    return {-1, move(child2)};
*/
}

Path Tsp_genetic::getTheFittestIndividual()
{
    int cost = population[0].cost;
    Path fittest = population[0];
    for (auto el : population)
    {
        if (el.cost < cost)
        {
            cost = el.cost;
            fittest = el;
        }
    }
    
    
    return fittest;
}

Path Tsp_genetic::searchTspGenetic(int sizePopulation, double crossOverRate, double mutationRate, int time){
    chrono::steady_clock::time_point startTime;
    chrono::steady_clock::time_point endTime;
    int currentTime=0;
    int counter = 0;
    int lastShowBestCost= 0;
    
    std::vector<Path> newPopulation;
    Path firstParent;
    Path secondParent;
    Path child;
    Path tmp;
    Path bestPath;
    
    numberOfTurnament = sizePopulation / 3;
    
    setPopulation(sizePopulation);
    setCrossOverRate(crossOverRate);
    setMutationRate(mutationRate);
    init();
    initPopulation(sizePopulation);
    countFitness();
    
    bestPath = getTheFittestIndividual();
    
    //for(int i=0; i<population.size(); i++){
    startTime = std::chrono::steady_clock::now();
    do{
            child=getTheFittestIndividual();
            newPopulation.push_back(child);
          
            
            
        for(int i=0; i<sizePopulation;i++){
        child.path.clear();
        firstParent=selectionUsingTournamentMethod();
        secondParent=selectionUsingTournamentMethod();
        
        child=doCrossoverPMX(firstParent.path, secondParent.path);
        
        if(getRandomFromZeroToOne()<crossOverRate)
            newPopulation.push_back(child);
            }
       
        for(auto el: newPopulation)
            if(getRandomFromZeroToOne()< mutationRate)
                mutate(el);
        
        
        population.swap(newPopulation);
        vector<Path>().swap(newPopulation);
        
        countFitness();
        child=getTheFittestIndividual();
        
        if(child.cost<bestPath.cost){
            bestPath=child;
        }
        
        endTime = std::chrono::steady_clock::now();
        currentTime = std::chrono::duration_cast<std::chrono::seconds>(endTime - startTime).count();
        
        if (currentTime >= counter)
        {
            if (lastShowBestCost != bestPath.cost)
            {
                cout<<currentTime<<endl;
                std::cout<<currentTime<<" sek. wynik: "<<bestPath.cost<<std::endl;
                lastShowBestCost = bestPath.cost;
            }
            counter++;
        }
        
    } while (currentTime < time);
    


    for(int i =0; i<citiesNumber; i++ ){
        cout<<bestPath.path[i]<<"-";
    }
    cout<<endl;
   
            
    
    return move(bestPath);
}

