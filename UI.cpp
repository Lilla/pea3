//
//  UI.cpp
//  algorytm_genetyczny
//
//  Created by Lila Łomnicka on 1/13/19.
//  Copyright © 2019 Lila. All rights reserved.
//

#include "UI.hpp"

void UserInterface::menu(){
    int choice;
    
    
    bool start=false;
    MatrixOfDistances* matrix = new MatrixOfDistances;
    int time = 0;
    Tsp_genetic tspGenetic(*matrix);
  
    double crossOverRate=0.8;
    double mutationRate=0.1;
    int populationSize=10;
    
    
    
    
    vector<int> path;
    
    
    
    
    
    while(!start){
        cout<<"M E N U"<<endl;
        cout<<"1-Load from file"<<endl;
        cout<<"2-set stop criterion"<<endl;
        cout<<"3-set crossOver rate size"<<endl;
        cout<<"4-set mutation rate"<<endl;
        cout<<"5-set population size "<<endl;
        cout<<"6-genetic algorithm start"<<endl;
        cout<<"0-exit"<<endl;
        
        cout<<"What would you like to do? "<<endl;
        
        cin>>choice;
        
        
        switch(choice){
            case 1:
            {
                string fileName;
                cout<<"File name "<<endl;
                cin>>fileName;
                matrix->loadFile(fileName);
                matrix->display();
                
            }
                break;
                
            case 2:
            {
                int intime;
                cout<<"Set time in seconds: "<<endl;
                cin>>intime;
                time=intime;
                
                
            }
                break;
                
            case 3:
            {
              /*  int size;
                cout<<"Set  temperature size: "<<endl;
                cin>>size;
                tspSA.setSizeTemp(size);*/
                
            }
                break;
                
            case 4:
            {
               /* double rate;
                cout<<"Set  temperature rate: "<<endl;
                cin>>rate;
                tspSA.setAlpha(rate);*/
            }
                break;
                
            case 5:
            {
              /*  double end;
                cout<<"Set end temperature : "<<endl;
                cin>>end;
                tspSA.setEndTempe(end);*/
            }
                break;
            case 6:
            {
                
                cout<<time;
                
                tspGenetic.searchTspGenetic(populationSize, crossOverRate, mutationRate, time);
                
            }
                
                
                break;
                
         
            case 0:
            {
                start=true;
            }
                break;
                
                
                
        }
        
    }
}
