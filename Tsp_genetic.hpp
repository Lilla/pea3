//
//  Tsp_genetic.hpp
//  algorytm_genetyczny
//
//  Created by Lila Łomnicka on 1/12/19.
//  Copyright © 2019 Lila. All rights reserved.
//

#ifndef Tsp_genetic_hpp
#define Tsp_genetic_hpp

#include <iostream>
#include "Path.hpp"
#include "MatrixOfDistances.hpp"
using namespace std;

class Tsp_genetic{
    vector<Path> population;
    double crossOverRate;
    double mutationRate;
    int populationSize;
    const MatrixOfDistances  &matrix;
    int citiesNumber;
    int numberOfTurnament;
    
    void setPopulation(int size){populationSize=size;}
    int getPopulation(){return populationSize;}
    
    void setCrossOverRate(int crossOverRate){this->crossOverRate=crossOverRate;}
    double getCrossOverRate(){return this->crossOverRate;}
    
    void setMutationRate(int mutationRate){this->mutationRate=mutationRate;}
    double getMutationRate(){return this->mutationRate;}
    
    double getRandomFromZeroToOne();
    
    void init();
    void initPopulation(int);
    
    const Path greedy(int);
    const Path randomPath();
    
    Path doCrossoverPMX(vector<int> &, vector<int> &);
    
    int countCost(vector<int> &);
    void countFitness();
    void mutate(Path &);
        
    Path selectionUsingTournamentMethod();
    Path getTheFittestIndividual();
public:
    Path searchTspGenetic(int, double, double, int);
    Tsp_genetic(const MatrixOfDistances &orginalMatrix) : matrix(orginalMatrix) {};
    ~Tsp_genetic();
    
};

#endif /* Tsp_genetic_hpp */
