//
//  UI.hpp
//  algorytm_genetyczny
//
//  Created by Lila Łomnicka on 1/13/19.
//  Copyright © 2019 Lila. All rights reserved.
//

#ifndef UI_hpp
#define UI_hpp

#include "Tsp_genetic.hpp"

#include <iostream>


using namespace std;
class UserInterface{
public:
    void menu();
    
};

#endif /* UI_hpp */
